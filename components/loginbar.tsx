import Link from 'next/link';
import { signIn, signOut, useSession } from 'next-auth/react';
import styles from '../styles/pages.module.css';

export default function LoginBar() {
  const { data: session } = useSession();
  return (
    <nav className={styles.loginbar}>
      {!session && (
        <Link href="/api/auth/signin">
          <a
            onClick={(e) => {
              e.preventDefault();
              signIn('google');
            }}
          >
            Sign In
          </a>
        </Link>
      )}

      {session && (
        <>
          {session.user?.email}:{' '}
          <Link href="/api/auth/signout">
            <a
              onClick={(e) => {
                e.preventDefault();
                signOut();
              }}
            >
              Sign Out
            </a>
          </Link>
        </>
      )}
    </nav>
  );
}
