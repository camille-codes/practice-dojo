import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/pages.module.css';
import LoginBar from './loginbar';

export const appName = 'Practice Dojo';
const siteTitle = `Camille's ${appName}`;

export default function Layout({
  children,
  home,
  hat,
}: {
  children: React.ReactNode;
  home?: boolean;
  hat?: boolean;
}) {
  return (
    <div className={styles.container}>
      <Head>
        <title>{siteTitle}</title>
        <meta
          name="description"
          content="Practice Jin Sei Ryu Karate techniques and kata"
        />
        <meta name="og:title" content={siteTitle} />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/favicon_io/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon_io/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon_io/favicon-16x16.png"
        />
        <link rel="manifest" href="/favicon_io/site.webmanifest" />
      </Head>

      <LoginBar />
      <main className={styles.main}>
        <header>
          <h1 className={styles.title}>{appName}</h1>
        </header>
        {children}
      </main>

      <footer className={styles.footer}>
        <p></p>
        {home ? (
          <>
            <Link href="https://camille.codes">
              <a>camille.codes</a>
            </Link>
          </>
        ) : hat ? (
          <>
            <Link href="/">
              <a>← Back to Home</a>
            </Link>
            <Link href="/hats/choose">
              <a>Choose Another Hat →</a>
            </Link>
          </>
        ) : (
          <>
            <Link href="/">
              <a>← Back to Home</a>
            </Link>
          </>
        )}
      </footer>
    </div>
  );
}
