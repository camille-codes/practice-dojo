import styles from '../styles/pages.module.css';

import type { ReactNode } from 'react';
import { Grade, MyData, ordinalNumberMap } from '../lib/types';

export function liClickedHandler(event: React.MouseEvent<HTMLLIElement>): void {
  const li = event.currentTarget;
  li.classList.toggle(`${styles.li_clicked}`);
}

export function displayItems({ data }: { data: MyData[] }): ReactNode {
  return data.map((dataItem) => {
    return (
      <li key={dataItem.id} className={styles.li} onClick={liClickedHandler}>
        {dataItem.rank} {dataItem.grade} {dataItem.name}
      </li>
    );
  });
}

function hatPageWelcome({ hatType }: { hatType: string }): JSX.Element {
  return (
    <>
      Welcome to the <br />
      <b>{hatType}</b> hat page!
    </>
  );
}

export function displayRandom({
  hatName,
  data,
}: {
  hatName: string;
  data: MyData[];
}): JSX.Element {
  return (
    <>
      <div className={styles.hat_title}>
        {hatPageWelcome({ hatType: hatName })}
      </div>

      <div className={styles.description}>
        Here is a random listing of available {hatName}:
        <ul className={styles.ul_li}>{displayItems({ data: data })}</ul>
      </div>
    </>
  );
}

export function displayAllRandom({
  hatName,
  data,
}: {
  hatName: string;
  data: MyData[];
}): JSX.Element {
  return (
    <>
      <div className={styles.hat_title}>
        {hatPageWelcome({ hatType: hatName })}
      </div>

      <div className={styles.description}>
        Here is a random listing of available kata and curriculum:
        <ul className={styles.ul_li}>{displayItems({ data: data })}</ul>
      </div>
    </>
  );
}

function orderByCompareFn(dataA: MyData, dataB: MyData): number {
  // return -1 if A is bigger, 1 if B is bigger, 0 if equal
  // pass the rank of dataA and dataB to something that gives back a number
  const rankDiff = ordinalNumberMap[dataB.rank] - ordinalNumberMap[dataA.rank];

  // If grade === Grade.dan, it should show up last
  if (dataA.grade !== dataB.grade) {
    if (dataA.grade === Grade.kyu) {
      return -1;
    }

    return 1;
  }

  if (rankDiff !== 0) {
    return rankDiff;
  }

  // Sort by series
  if (dataA.series !== dataB.series) {
    // return -1 if A.series < B.series, 1 if B.series < A.series
    return dataA.series < dataB.series ? -1 : 1;
  }

  // If the series is the same, sort by order
  if (dataA.order !== undefined && dataB.order !== undefined) {
    return dataA.order - dataB.order;
  }

  return 0;
}

function orderByRank(data: MyData[]): MyData[] {
  return data.sort(orderByCompareFn);
}

export function displayAllNonRandom({
  hatName,
  dataKata,
  dataCurriculum,
}: {
  hatName: string;
  dataKata: MyData[];
  dataCurriculum: MyData[];
}): JSX.Element {
  const orderedKata = orderByRank(dataKata);
  const orderedCurriculum = orderByRank(dataCurriculum);

  return (
    <>
      <div className={styles.hat_title}>
        {hatPageWelcome({ hatType: hatName })}
      </div>

      <div className={styles.description}>
        Here is a listing of available kata:
        <ul className={styles.ul_li}>{displayItems({ data: orderedKata })}</ul>
        Here is a listing of available curriculum:
        <ul className={styles.ul_li}>
          {displayItems({ data: orderedCurriculum })}
        </ul>
      </div>
    </>
  );
}

export function randomizer<Type>({ data }: { data: Type[] }): Type[] {
  // Create a copy of the array
  let dataCopy = data.slice(0);

  // While the array still has elements, add random element to new array
  let randomData: Type[] = [];

  while (dataCopy.length > 0) {
    let elementIndex = Math.floor(Math.random() * dataCopy.length);
    let item = dataCopy[elementIndex];
    //console.log(item);
    randomData.push(item);
    dataCopy.splice(elementIndex, 1);
  }

  //console.log('Hat empty.');

  return randomData;
}

function combineData<Type>({
  dataOne,
  dataTwo,
}: {
  dataOne: Type[];
  dataTwo: Type[];
}): Type[] {
  // Alternate adding an element from each data structure to a new combined structure
  let combinedData: Type[] = [];

  dataOne.forEach((itemOne, index) => {
    combinedData.push(itemOne);

    let itemTwo = dataTwo[index];

    if (itemTwo === undefined) {
      // console.log(`${dataTwo} have all been added already.`);
    } else {
      combinedData.push(itemTwo);
    }
  });

  return combinedData;
}

// ? What if we have more than two to interleave?
// TODO Make this take an array of arrays
export function interleave<Type>({
  dataA,
  dataB,
}: {
  dataA: Type[];
  dataB: Type[];
}) {
  // Determine which data structure is larger and assign the resulting combined data
  let result: Type[] = [];

  if (dataA.length > dataB.length) {
    result = combineData({ dataOne: dataA, dataTwo: dataB });
  } else {
    result = combineData({ dataOne: dataB, dataTwo: dataA });
  }

  return result;
}
