import Link from 'next/link';
import { ReactElement } from 'react';
import Layout from '../components/layout';
import styles from '../styles/pages.module.css';

export default function Custom404() {
  return (
    <>
      <h1>404: Page Not Found</h1>
      <p className={styles.description}>
        Are you trying to make a{' '}
        <Link href="/choose">
          <a>hat</a>
        </Link>
        ?
      </p>
    </>
  );
}

Custom404.getLayout = function getLayout(error: ReactElement) {
  return <Layout>{error}</Layout>;
};
