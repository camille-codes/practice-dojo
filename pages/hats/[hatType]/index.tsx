import Layout from '../../../components/layout';
import styles from '../../../styles/pages.module.css';

import { useRouter } from 'next/router';
import { ReactElement } from 'react';
import { InferGetServerSidePropsType } from 'next/types';
import { getServerSideProps as _getServerSideProps } from '../../../lib/utils';

import {
  randomizer,
  interleave,
  displayRandom,
  displayAllRandom,
} from '../../../components/pages';

export default function HatType({
  jsonData,
}: InferGetServerSidePropsType<typeof _getServerSideProps>): JSX.Element {
  // console.log({ jsonData });
  const router = useRouter();
  const hatType = router.query.hatType;

  const randomKata = randomizer({ data: jsonData.allKata });
  const randomCurriculum = randomizer({ data: jsonData.allCurriculum });
  const combinedKataCurriculum = interleave({
    dataA: randomKata,
    dataB: randomCurriculum,
  });

  switch (hatType) {
    case 'kata':
      // console.log('Display kata only');
      return <>{displayRandom({ hatName: hatType, data: randomKata })}</>;
    case 'curriculum':
      // console.log('Display curriculum only');
      return <>{displayRandom({ hatName: hatType, data: randomCurriculum })}</>;
    case 'review all':
      // console.log('Display both curriculum and kata');
      return (
        <>
          {displayAllRandom({ hatName: hatType, data: combinedKataCurriculum })}
        </>
      );
    default:
      // console.log('Display error page')
      return (
        <div className={styles.hat_title}>
          <b>404</b>: You're trying to make a <b>{hatType}</b> hat that I am not
          equipped to make.
        </div>
      );
  }
}

HatType.getLayout = function getLayout(hat: ReactElement) {
  return <Layout hat>{hat}</Layout>;
};

export const getServerSideProps = _getServerSideProps;
