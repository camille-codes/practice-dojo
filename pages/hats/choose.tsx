import { useRouter } from 'next/router';
import { ReactElement } from 'react';
import Layout from '../../components/layout';
import styles from '../../styles/pages.module.css';

export default function ChooseHatPage() {
  const router = useRouter();

  const curriculumButtonHandler = () => {
    // console.log(`Here is the button click for the technique hat`);
    /*
    On button click, all the techniques are gathered, 
    randomized, and then returned in a random order to the user,
    user gets taken to another screen with the hat that was created
     */
    router.push({ pathname: '/hats/curriculum' });
  };
  const kataButtonHandler = () => {
    // console.log(`Here is the button click for the kata hat`);
    /*
    On button click, all the katas are gathered, 
    randomized, and then returned in a random order to the user,
    user gets taken to another screen with the hat that was created
     */
    router.push({ pathname: '/hats/kata' });
  };
  const reviewAllButtonHandler = () => {
    // console.log(`Here is the button click for the everything hat`);
    /*
    On button click, all the techniques and katas are gathered, 
    randomized, and then returned in a random order to the user,
    alternating between kata and techniques,
    user gets taken to another screen with the hat that was created
     */
    router.push({ pathname: '/hats/review all' });
  };
  const nonRandomButtonHandler = () => {
    // console.log(`Here is the button click for the NON-random everything hat`);
    /*
    On button click, all the techniques and katas are gathered and then 
    returned to the user,
    user gets taken to another screen with the hat that was created
     */
    router.push({ pathname: '/hats' });
  };

  return (
    <>
      <div className={styles.description}>What type of hat would you like?</div>

      <button className={styles.button} onClick={curriculumButtonHandler}>
        Curriculum <br />
        (random)
      </button>
      <br />
      <button className={styles.button} onClick={kataButtonHandler}>
        Kata <br />
        (random)
      </button>
      <br />
      <button className={styles.button} onClick={reviewAllButtonHandler}>
        All <br />
        (random)
      </button>
      <br />
      <button className={styles.button} onClick={nonRandomButtonHandler}>
        All
        <br />
        (non-random)
      </button>
    </>
  );
}

ChooseHatPage.getLayout = function getLayout(choose: ReactElement) {
  return <Layout>{choose}</Layout>;
};
