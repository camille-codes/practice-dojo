import Layout from '../../components/layout';
import { ReactElement } from 'react';

import { getServerSideProps as _getServerSideProps } from '../../lib/utils';
import { InferGetServerSidePropsType } from 'next/types';

import { displayAllNonRandom } from '../../components/pages';

export default function Hats({
  jsonData,
}: InferGetServerSidePropsType<typeof _getServerSideProps>): JSX.Element {
  // console.log({ jsonData });

  return (
    <>
      {displayAllNonRandom({
        hatName: 'non-random',
        dataKata: jsonData.allKata,
        dataCurriculum: jsonData.allCurriculum,
      })}
    </>
  );
}

Hats.getLayout = function getLayout(hat: ReactElement) {
  return <Layout hat>{hat}</Layout>;
};

export const getServerSideProps = _getServerSideProps;
