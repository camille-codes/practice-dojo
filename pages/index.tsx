import Link from 'next/link';
import { ReactElement } from 'react';

import Layout, { appName } from '../components/layout';
import styles from '../styles/pages.module.css';

export default function HomePage() {
  return (
    <>
      <div className={styles.title}>Welcome to {appName}!</div>

      <div className={styles.description}>
        Want to make a{' '}
        <Link href="/hats/choose">
          <a>hat</a>
        </Link>
        ?
      </div>
    </>
  );
}

HomePage.getLayout = function getLayout(home: ReactElement) {
  return <Layout home>{home}</Layout>;
};
