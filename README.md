# Practice Dojo

An app to help me practice karate

## Run locally

    npm run dev

## Run TS file

    npx tsc '<filepath>.ts'
    node '<filepath>.js'

or (preferred)

    ts-node

## Commit Emoji Key

    📝      Documentation or comments added
    🎉      New feature started
    🔧      Code added
    ♻       Code refactored
    🚚      Moved or renamed something
    ✔       Worked on checks or adjusted code to be compliant
    🐛      Fixed a bug
    🎨      Added theme or layout
    💄       Improved UI
    🔥       Removed something
