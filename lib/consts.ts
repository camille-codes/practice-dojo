import { Series, Syllabi } from '../data/syllabus';
import { OrdinalNumber, Belt, Grade, MyData, FetchedData } from './types';

export const authorizedUsers = [
  process.env.AUTHORIZED_USER_ONE,
  process.env.AUTHORIZED_USER_TWO,
];

const allKata: MyData[] = [
  {
    id: 'kata 1',
    name: 'Taikyoku Sono Ichi',
  },
  {
    id: 'kata 2',
    name: 'Taikyoku Sono Ni',
  },
  {
    id: 'kata 3',
    name: 'Taikyoku Sono San',
  },
  {
    id: 'kata 4',
    name: 'Pinan Sono Ichi',
  },
  {
    id: 'kata 5',
    name: 'Pinan Sono Ni',
  },
  {
    id: 'kata 6',
    name: 'Pinan Sono San',
  },
  {
    id: 'kata 7',
    name: 'Pinan Sono Yon',
  },
  {
    id: 'kata 8',
    name: 'Pinan Sono Go',
  },
].map((item) => ({
  ...item,
  belt: Belt.white,
  rank: OrdinalNumber.tenth,
  grade: Grade.kyu,
  series: Series.taikyoku,
  syllabi: Syllabi.kata,
}));

const allCurriculum: MyData[] = [
  {
    id: 'kihon 1',
    name: 'Kihon Kumite 1',
  },
  {
    id: 'kihon 2',
    name: 'Kihon Kumite 2',
  },
  {
    id: 'kihon 3',
    name: 'Kihon Kumite 3',
  },
  {
    id: 'kihon 4',
    name: 'Kihon Kumite 4',
  },
  {
    id: 'kihon 5',
    name: 'Kihon Kumite 5',
  },
  {
    id: 'kihon 6',
    name: 'Kihon Kumite 6',
  },
  {
    id: 'kihon 7',
    name: 'Kihon Kumite 7',
  },
].map((item) => ({
  ...item,
  belt: Belt.white,
  rank: OrdinalNumber.tenth,
  grade: Grade.kyu,
  series: Series.kihon,
  syllabi: Syllabi.kihon,
}));

export const constData: FetchedData = {
  allKata,
  allCurriculum,
};
