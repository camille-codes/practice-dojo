import { constData } from '../consts';
import { FetchedData } from '../types';

import { firestoreDataService } from './firestore.service';

export interface DataService {
  getData(): Promise<FetchedData>;
}

export const apiDataService: DataService = {
  getData: async () => {
    const response = await fetch(process.env.DATABASE_URL!);
    const data: FetchedData = await response.json();
    console.log(data);

    return data;
  },
};

export const constDataService: DataService = {
  getData: () => {
    return Promise.resolve(constData);
  },
};

// TODO Implement properly
export const databaseDataService = apiDataService;

// Factory
export function getDataService(isAuthenticated: boolean): DataService {
  if (isAuthenticated) {
    return firestoreDataService;
  }

  return constDataService;
}
