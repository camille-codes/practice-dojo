import { DataService } from '.';
import { FetchedData, MyData, ToDatabaseData } from '../types';

import { applicationDefault, initializeApp } from 'firebase-admin/app';
import { FirestoreDataConverter, getFirestore } from 'firebase-admin/firestore';

try {
  console.log('Running firestore service.');

  initializeApp({
    credential: applicationDefault(),
  });
} catch (error) {
  console.log('Application instance already created.');
}

const db = getFirestore();

export const firestoreDataService: DataService = {
  getData: async () => {
    const myDataConverter = new MyDataConverter();
    const syllabusRef = db.collection('syllabus');

    const kataPromise = syllabusRef
      .where('syllabi', '==', 'kata')
      .withConverter(myDataConverter)
      .get();
    const curriculumPromise = syllabusRef
      .where('syllabi', 'in', ['self-defense', 'kihon'])
      .withConverter(myDataConverter)
      .get();

    const [kataSnapshot, curriculumSnapshot] = await Promise.all([
      kataPromise,
      curriculumPromise,
    ]);

    const allKata: MyData[] = kataSnapshot.docs.map((doc) => doc.data());
    const allCurriculum: MyData[] = curriculumSnapshot.docs.map((doc) =>
      doc.data()
    );

    const combinedData: FetchedData = { allKata, allCurriculum };
    return combinedData;
  },
};

class MyDataConverter implements FirestoreDataConverter<MyData> {
  toFirestore(modelObject: MyData): FirebaseFirestore.DocumentData;
  toFirestore(
    modelObject: Partial<MyData>,
    options: FirebaseFirestore.SetOptions
  ): FirebaseFirestore.DocumentData;
  toFirestore(modelObject: any, options?: any): FirebaseFirestore.DocumentData {
    // Remove the 'id' field if it exists
    if ('id' in modelObject) {
      const { id, ...rest } = modelObject;
      return rest;
    }

    return modelObject;
  }
  fromFirestore(
    snapshot: FirebaseFirestore.QueryDocumentSnapshot<FirebaseFirestore.DocumentData>
  ): MyData {
    const data = snapshot.data() as Omit<MyData, 'id'>;

    const myData: MyData = {
      id: snapshot.id,
      ...data,
    };

    return myData;
  }
}

/** Create new entries in the database */
export async function createEntries(entries: ToDatabaseData[]): Promise<void> {
  const myDataConverter = new MyDataConverter();
  const batch = db.batch();

  entries.forEach((entry) => {
    const docRef = db
      .collection('syllabus')
      .doc()
      .withConverter(myDataConverter);
    batch.create(docRef, entry);
  });

  await batch.commit();
}
