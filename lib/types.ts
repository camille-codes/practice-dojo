import { Series, Syllabi } from '../data/syllabus';

export enum HatTypes {
  curriculum = 'curriculum',
  kata = 'kata',
  reviewAll = 'review all',
}

export enum Grade {
  kyu = 'kyu',
  dan = 'dan',
}

export enum Number {
  one = '1',
  two = '2',
  three = '3',
  four = '4',
  five = '5',
  six = '6',
  seven = '7',
  eight = '8',
  nine = '9',
  ten = '10',
}

export enum OrdinalNumber {
  tenth = '10th',
  ninth = '9th',
  eighth = '8th',
  seventh = '7th',
  sixth = '6th',
  fifth = '5th',
  fourth = '4th',
  third = '3rd',
  second = '2nd',
  first = '1st',
}

export const ordinalNumberMap = {
  [OrdinalNumber.tenth]: 10,
  [OrdinalNumber.ninth]: 9,
  [OrdinalNumber.eighth]: 8,
  [OrdinalNumber.seventh]: 7,
  [OrdinalNumber.sixth]: 6,
  [OrdinalNumber.fifth]: 5,
  [OrdinalNumber.fourth]: 4,
  [OrdinalNumber.third]: 3,
  [OrdinalNumber.second]: 2,
  [OrdinalNumber.first]: 1,
};

export enum Belt {
  white = 'white',
  advWhite = 'advanced white',
  blue = 'blue',
  advBlue = 'advanced blue',
  yellow = 'yellow',
  advYellow = 'advanced yellow',
  green = 'green',
  advGreen = 'advanced green',
  brown = 'brown',
  advBrown = 'advanced brown',
  black = 'black',
}

export type MyData = {
  id: string;
  name: string;
  belt: Belt;
  rank: OrdinalNumber;
  grade: Grade;
  series: Series;
  order?: number;
  syllabi: Syllabi;
};

export type ToDatabaseData = Omit<MyData, 'id'>;

export type FetchedData = {
  allKata: MyData[];
  allCurriculum: MyData[];
};
