import { GetSessionParams, getSession } from 'next-auth/react';
import { authorizedUsers } from './consts';
import { getDataService } from './services';

export async function getServerSideProps(context: GetSessionParams) {
  const session = await getSession(context);
  const isAuthenticated =
    !!session?.user?.email && authorizedUsers.includes(session.user.email);
  // '!!' forces a boolean type

  const dataService = getDataService(isAuthenticated);

  const data = await dataService.getData();
  // console.log(data);

  return {
    props: {
      jsonData: data,
    },
  };
}
